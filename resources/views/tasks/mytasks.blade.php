@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
    @csrf
    <div class="container-fluid mt--7" style="padding-top:50px">
     <div class="row">
      <div class="col">
        <div class="card-shadow">
            <div class="card-header border-0" >
                        <div class="row align-items-center">
                            <div class="col-11">
                                 <h2 class="mb-2" style="text-align:center">This is your task list</h2>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                <table class="table align-items-center table-flush" style="text-align:center">
                    <thead class="thead-light">
                            <tr>
                            <th scope="col">Task's Status</th>
                            <th scope="col">Task's Description</th>
                            <th scope="col">Meeting's Title</th>
                            <th scope="col">Task Deadline</th>
                            </tr>
                    </thead>

                    @foreach($tasks as $task)
                    <tr>
                <td>  @if ($task->status)
                        <input type = 'checkbox' id ="{{$task->id}}" checked>
                    @else
                        <input type = 'checkbox' id ="{{$task->id}}">
                    @endif</td>
                    <td>
                    @php 
                        date_default_timezone_set("Asia/Jerusalem");
                        $currentTime = now();
                        $currentDate = $currentTime->format('Y-m-d');
                            @endphp
                        @if($task->task_deadline<$currentDate && $task->status == 0)
                    
                        <a style="color: red; font-weight:bold">{{$task->title}}</a>
                    @else
                        <a style="color: green">{{$task->title}}</a>
                    
                    @endif
                    </td>
                    <td><a>{{$task->meeting->title}}</a></td>
                    <td><a>{{$task->task_deadline}}</a></td>
                    </tr>
                    @endforeach
                </table>
                </div>

    </div>
    </div>
    </div>
    </div>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('tasks')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'PUT',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>
@endsection