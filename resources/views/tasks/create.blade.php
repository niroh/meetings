
@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
<h1>Create New task</h1>
<form method="POST" action="{{ route('storetask', $id1) }}" accept-charset="UTF-8">
@csrf
<div class = "form-group">
    <label for = "title">Which task would you like to add?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<label for = "user_id">User Responsibility</label>
<div class = "form-group">
<select class="form-control" name = "user_id">
@foreach($org_users as $org_user)
      <option>{{$org_user->id}}</option>
    @endforeach
  </select>
  </div>
  @php $c =new DateTime('now');
    $cformat = $c->format('Y-m-d');
    @endphp
  <div class = "form-group">
    <label for = "task_deadline">Task Deadline</label>
    <input type= "date" class = "form-control" name= "task_deadline" min="{{$cformat}}">
</div>

    <input name="_method" type="hidden" value="GET">
    {{ csrf_field() }}

    <button type="submit" class="btn btn-sm btn-default">Add Task</button>
<input type="hidden" value="id1" />
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</form>


<!-- <form method = 'post' action="{{action('TaskController@store',$id1)}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">Which task would you like to add?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

<input type = "hidden" value = "id">

</form> -->
@endsection