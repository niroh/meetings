
@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
<h1>Edit task</h1>
<form method = 'post' action="{{action('TaskController@update', $tasks->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Task to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$tasks->title}}">
</div>
<label for = "meeting_id">Move To Other Meeting:</label>
<div class = "form-group">
<select class="form-control" name = "meeting_id"  value= "{{$tasks->meeting_id}}">
@foreach($meetings1 as $meeting)
      <option>{{$meeting->id}}</option>
    @endforeach
  </select>
  </div>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>

<form method = 'post' action="{{action('TaskController@destroy', $tasks->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Task">
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</form>
@endsection

