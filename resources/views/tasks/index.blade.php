@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
    @csrf
    <div class="container-fluid mt--7" style="padding-top:50px">
     <div class="row">
      <div class="col">
        <div class="card-shadow">
                <div class="card-header border-0" >
                    <div class="row align-items-center">
                        <div class="col-11">
                            <h2 class="mb-2" style="text-align:center">Subjects to be discussed</h2>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush" style="text-align:center">
                        <thead class="thead-light" >
                            <tr>
                            <th scope="col">Subject's Status</th>
                            <th scope="col">Title</th>
                            <th scope="col">Due To Hour</th>
                            <th scope="col">Mark As Done</th>
                            </tr>
                            <tr>
                        </thead>
                            @foreach($subjects as $subject)
                            <td>    
                            @if($subject->status)
                                    <input type = 'checkbox' id="{{$subject->id}}" checked>
                                @else
                                    <input type = 'checkbox' id="{{$subject->id}}">
                                    @endif
                                    </td>
                                <td><a>{{$subject->title}} </a>
                                </td>
                                <td><a>{{$subject->subject_time}}</a></td>
                                @php 
                                    date_default_timezone_set("Asia/Jerusalem");
                                    $currentTime = now();
                                    $currentDate = $currentTime->format('Y-m-d');
                                    $currentTime1 =$currentTime->format('H:i:sa');
                                        @endphp
                            @if($subject->subject_time < $currentTime1 && $subject->meeting->start_hour < $currentTime1 && $subject->meeting->ending_hour > $currentTime1 && $subject->meeting->meeting_date == $currentDate && $subject->status==0)
                            <td> <a href="{{route('updatesubject',$subject->id)}}"> Mark As Done</a></td>
                            @elseif(!($subject->subject_time < $currentTime1 && $subject->meeting->start_hour < $currentTime1 && $subject->meeting->ending_hour > $currentTime1 && $subject->meeting->meeting_date == $currentDate))
                            <td><a>The meeting is not happaning right now</a></td> 
                            @else
                            <td><a>Done</a> </td>
                            @endif
                            <tr>
                            @endforeach
                            </table>
                    </div>
       </div>
      </div>
     </div>
    </div>






    <div class="container-fluid mt--7" style="padding-top:200px">
     <div class="row">
      <div class="col">
        <div class="card-shadow">
                 <div class="card-header border-0" >
                    <div class="row align-items-center">
                        <div class="col-11">
                        <h2 class="mb-2" style="text-align:center">Tasks To Complete</h1>
                        </div>
                      </div>
                  </div>

                  <div class="table-responsive">
                    <table class="table align-items-center table-flush" style="text-align:center">
                        <thead class="thead-light" >
                                <tr>
                                    <th scope="col"> Task's Status</th>  
                                    <th scope="col"> Task Description</th>
                                    <th scope="col"> User Responsable</th>
                                    <th scope="col"> Task Deadline</th> 
                                </tr>
                            </thead> 

                            @foreach($tasks as $task)
                            <tr>
                        <td>
                        @can('participant')
                            @if($task->user_id==$user)
                            @if($task->status)
                                <input type = 'checkbox' id="{{$task->id}}" checked>
                            @else
                                <input type = 'checkbox' id="{{$task->id}}">
                            @endif</td>
                            <td>
                            @if($task->task_deadline < $currentDate) <a style="color: red; font-weight:bold" href= "{{route('tasks.edit', $task->id )}}">{{$task->title}} </a>
                            @else
                            <a style="color: green" href= "{{route('tasks.edit', $task->id )}}">{{$task->title}} </a>
                            @endif
                        </td>
                            <td><a>{{$task->user_id}}</a></td>
                            @endif
                            @endcan
                            </tr>
                        <tr>
                            
                        <td>
                            @if($task->task_deadline < $currentDate && $task->status==0)
                            @if($task->status)
                                <input type = 'checkbox' id="{{$task->id}}" checked>
                            @else
                                <input type = 'checkbox' id="{{$task->id}}">
                            @endif
                            </td>
                            @cannot('participant')
                            <td>
                            <a  style="color: red; font-weight:bold" href= "{{route('tasks.edit', $task->id )}}">{{$task->title}} </a>
                            @endcannot
                        </td>
                            <td><a  style="color: red; font-weight:bold">{{$task->user_id}}</a></td>
                            <td>
                            <a  style="color: red; font-weight:bold">{{$task->task_deadline}}</a></td>
                            @else

                            @if($task->status)
                                <input type = 'checkbox' id="{{$task->id}}" checked>
                            @else
                                <input type = 'checkbox' id="{{$task->id}}">
                            @endif
                            </td>
                            @cannot('participant')
                            <td>
                            <a  style="color: green" href= "{{route('tasks.edit', $task->id )}}">{{$task->title}} </a>
                            @endcannot
                            </td>
                            <td><a style="color: green">{{$task->user_id}}</a></td>
                            <td>
                            <a style="color: green">{{$task->task_deadline}}</a></td>
                            @endif
                        </tr>
                        @endforeach
                        </table>
                    </div>
        </div>
      </div>
     </div>
    </div>


<a href="{{route('createtask',$id)}}" class="btn btn-info" style="margin-left: 450px">Create New Task </a>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('tasks')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'PUT',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>
@endsection