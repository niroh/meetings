@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')
@csrf
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Organization Statistics</div>

                <div class="panel-body">
                    {!! $chart->html() !!}
                </div>
                <hr>
                {!! $pie->html() !!}
              
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}
{!! $chart->script() !!}
{!! $pie->script() !!}

@endsection