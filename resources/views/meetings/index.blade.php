    @extends('layouts.app', ['title' => __('User Management')])

    @section('content')
        @include('layouts.headers.cards')
        @csrf
 <div class="container-fluid mt--7" style="padding-top:50px">
    <div class="row">
      <div class="col">
        <div class="card-shadow">
              <div class="card-header border-0" >
                <div class="row align-items-center">
                  <div class="col-11">    
                    <h2 class="mb-2" style="text-align:center" >This is your meetings list</h1>
                  </div>
                </div>
               </div> 

          
                    <div class="table-responsive">
                    <table class="table align-items-center table-flush" style="text-align:center">
                        <thead class="thead-light" >
                              <tr>
                                <th scope="col">Meeting Number</th>
                                <th scope="col">Meeting Title</th>
                                <th scope="col">Meeting Creator</th>
                                <th scope="col">Meeting Date</th>
                                <th scope="col">Start Time</th>
                                <th scope="col">Ending Time</th>
                                <th scope="col">Place</th>
                                <th scope="col"></th>
                              </tr>
                          </thead>

                          <tbody >
                        @foreach($meetings as $meeting)
                        @foreach($parts as $part)
                        @if($meeting->id==$part->meeting_id)
                        <tr>
                          <td>{{$meeting->id}}</td>
                          <td>@cannot('participant')<a href= "{{route('showtasks', $meeting->id )}}">@endcannot {{$meeting->title}} </a></td>
                          <td>{{$meeting->user->name}}</td>
                          <td>{{$meeting->meeting_date}}</td>
                          <td>{{$meeting->start_hour}}</td>
                          <td>{{$meeting->ending_hour}}</td>
                          <td>{{$meeting->place}}</td>
                          @cannot('participant')<td><a href= "{{route('meetings.edit', $meeting->id )}}"> Edit </a></td>@endcannot
                        </tr>
                        @endif
                        @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
   </div>
  </div>
 </div>
</div>
    @cannot('participant')<a href="{{route('meetings.create')}}" class="btn btn-info" style="margin-left: 450px"> Create New Meeting </a> @endcannot
    @endsection