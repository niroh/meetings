@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
    @csrf
<h1>Edit a Meeting</h1>
<form method = 'post' action="{{action('MeetingsController@update', $meetings->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Meeting to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$meetings->title}}">
</div>
<div class = "form-group">
    <label for = "meeting_date">Meeting's Time to Update</label>
    <input type= "date" class = "form-control" name= "meeting_date" value = "{{$meetings->meeting_date}}">
</div>
<div class="row">
    <div class="col-6 col-md-4"  style="margin-left:120px">
        <label for = "start_hour">From</label>
        <input type= "time" class = "form-control timepicker" name= "start_hour" min="08:00" value="{{$meetings->start_hour}}">
    </div>
    <div class = "col-6 col-md-4" style="margin-left:60px">
        <label for = "ending_hour">To</label>
        <input type= "time" class = "form-control" name= "ending_hour" max="20:00"value="{{$meetings->ending_hour}}">
    </div>
</div> 

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save Changes">
</div>

</form>
<form method = 'post' action="{{action('MeetingsController@destroy', $meetings->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete a Meeting">
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</form>

@endsection