@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')
    @csrf
<h1>Create a New Meeting</h1>
<form method = 'post' action="{{action('MeetingsController@store')}}">
{{csrf_field()}}

<div class="row">
    <div class="col-6 col-md-4"  style="margin-left:120px">
        <label for = "title">Meeting's title</label>
        <input type= "text" class = "form-control" name= "title">
    </div>
            @php $c =new DateTime('now');
            $cformat = $c->format('Y-m-d');
            @endphp
    <div class = "col-6 col-md-4" style="margin-left:60px">
        <label for = "meeting_date">Meeting's Date</label>
        <input type= "date" class = "form-control" name= "meeting_date" min = {{$cformat}}>
    </div>
</div>

<div class="row">
    <div class="col-6 col-md-4"  style="margin-left:120px">
        <label for = "start_hour">From</label>
        <input type= "time" class = "form-control timepicker" name= "start_hour" min="08:00">
    </div>
    <div class = "col-6 col-md-4" style="margin-left:60px">
        <label for = "ending_hour">To</label>
        <input type= "time" class = "form-control" name= "ending_hour" max="20:00">
    </div>
</div>  
<div class="row">
<div class="col-6 col-md-4"  style="margin: auto">
    <label for = "place">Place</label>
    <input type= "text" class = "form-control" name= "place">
</div>
</div>
@can('admin')
<div class="row">
<div class="col-6 col-md-4"  style="margin: auto">
    <label for = "min_subject">Minumum Subject Per Meeting</label>
    <input type= "integer" class = "form-control" name= "min_subject" min='1' max='10'>
</div>
</div>@endcan

<div class="col-12 col-md-4"  style="margin: auto">
          <label class="form-control-label" for="add_participants">{{ __('Participants') }}</label>
            <select class="form-control m-bot15" name="add_participants[]" class="form-control form-control-alternative" placeholder="{{ __('Invited Participants') }}" value="" required>
             @foreach($users as $user)   
             
             <option  value="{{ $user->id }}" {{ $selectedRole = $user->id ? 'selected="selected"' : '' }}>{{ $user->name }}</option> 
             
              @endforeach
             </select>
</div>    
          <div id='add_participants' >
              </div>
<br>>
     <div  id ="participant_button">
     <a class="btn btn-info mt-2" style="margin-left:450px">{{ __('Add participants') }}</a>
    </div><br>


    @for($i=0; $i < $min_subjects ; $i++ )
    <div class="col-12 col-md-4"  style="margin: auto">
        <label class="form-control-label" for="add_subjects">{{ __('Subject') }}</label>
        <input type="text" name="add_subjects[]"  class="form-control form-control-alternative" placeholder="{{ __('Meeting Subjects') }}" value="" required><br>
        <input type="time" name="subject_hour[]"  class="form-control form-control-alternative"  value="" required><br>
    </div>
    @endfor
    <div id='add_subject'>
    </div>
    <div  id ="subject_button">
     <a class="btn btn-info mt-2"  style="margin-left:450px" >{{ __('Add Subject') }}</a>
    </div>
    <br><br>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-10 col-md-10"  style="margin-left:450px">
    <input type ="submit" class = "btn btn-info mt-2" name="submit" value ="Scheduele a Meeting">
</div>


<script>
                function createTicketComponent(type) {
                type = type || null;
                
                var elements   = [],
                    rootElement = document.createElement('p');
                        
                elements.push('<label class="form-control-label" for="add_participants">{{ __('Participants') }}</label>');
                elements.push('<select class="form-control m-bot15" name="add_participants[]" class="form-control form-control-alternative" placeholder="{{ __('Invited Participants') }}" value="" required>@foreach($users as $user)<option value="{{ $user->id }}" {{ $selectedRole = $user->id ? 'selected="selected"' : '' }}>{{ $user->name }}</option> @endforeach</select><br>');
                
                    
                rootElement.innerHTML = elements.join('');
                
                return rootElement;
                }

                function onClickCreateTicketButton(event) {
                var button    = event.target,
                    container = document.querySelector('#add_participants'),
                    component;

                
                    component = createTicketComponent();
                

                container.appendChild(component);
                }
                var buttonsGroup = document.getElementById('participant_button');
                buttonsGroup.addEventListener('click', onClickCreateTicketButton);
                

                //////////////////////////////////////////////////


                function createTicketComponentSubject(type) {
                type = type || null;
                
                var elements   = [],
                    rootElement = document.createElement('p');
                        
                elements.push('<label class="form-control-label" for="add_subjects">{{ __('Subjects') }}</label><input type ="text" name="add_subjects[]" class="form-control form-control-alternative" placeholder="{{ __('Meeting Subjects') }}" value=""required></br>');
                elements.push('<input type="time" name="subject_hour[]"  class="form-control form-control-alternative"  value="" required><br>');
                    
                rootElement.innerHTML = elements.join('');
                
                return rootElement;
                }

                function onClickCreateTicketButtonSubject(event) {
                var button    = event.target,
                    container = document.querySelector('#add_subject'),
                    component;

                
                    component = createTicketComponentSubject();
                

                container.appendChild(component);
                }
                var buttonsGroup = document.getElementById('subject_button');
                buttonsGroup.addEventListener('click', onClickCreateTicketButtonSubject);


</script>
</form>
@endsection