<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('meetings', 'MeetingsController')->middleware('auth');
Route::resource('tasks', 'TaskController')->middleware('auth');
Route::get('/showtasks/{id}', 'TaskController@index')->name('showtasks')->middleware('auth');
Route::get('createtask/{id}', 'TaskController@create')->name('createtask')->middleware('auth');
Route::get('mytasks', 'TaskController@mytasks')->name('mytasks')->middleware('auth');
//Route::get('/createnewtask/{id}', 'TaskController@store')->name('createnewtask');
Route::get('subject/update/{id}', 'TaskController@updatesubject')->name('updatesubject')->middleware('auth');
Auth::routes();
Route::get('/storetask/{id1}', 'TaskController@store')->name('storetask')->middleware('auth');
Route::get('/registeradmin','Auth\RegisterController@createadmin')->name('createadmin');
//Route::get('/storeadmin','Auth\RegisterController@storeadmin')->name('storeadmin');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Auth::routes();
//Route::resource('/register1/{user}','Auth\RegisterController@index')->name('createuser');
//Route::resource('/register2','Auth\RegisterController@create')->name('createuser2');
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('chart', 'ChartController@index')->name('chart')->middleware('auth');
Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

