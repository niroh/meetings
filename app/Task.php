<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }
    public function user_respo(){
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'title','id','organization_id','meeting_id', 'status','user_respo'
    ];

}
