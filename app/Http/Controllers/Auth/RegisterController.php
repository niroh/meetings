<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Organization;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/meetings';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'alpha', 'max:255' ,'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'min_subject'=>['required','integer','max:10'],
            'organization_name'=>['required', 'alpha', 'max:255' ,'unique:organizations'],
            'organization_id'=>['required','integer'],
        ]);
    }
   // public function index($user){
      //      $org_id=$user;
    //    return view('organization.create',compact('org_id')); 

  //  }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       $user = new User();
       $user->name = $data['name'];
       $user->email = $data['email'];
       $user->role = 'participant';
      // $user->organization_id =$orgid->id;
        $user->organization_id =$data['organization_id'];
       $user->password = Hash::make($data['password']);
       $user->save();
       return $user;
    //}
    }

    public function createadmin()
    {
        return view ('auth.registeradmin');
    }
    protected function storeadmin(array $data)
    {
       $user = new User();
       $user->name = $data['name'];
       $user->email = $data['email'];
       $user->role = 'admin';
       $user->organization_id =999;
       $user->password = Hash::make($data['password']);
       $user->save();
       

       $organization = new Organization();
       $organization->name = $data['organization_name'];
       $organization->user_id = $user->id;
       $organization->min_subject= $data['min_subject'];
       $organization->save();

       $user->organization_id = $organization->id;
       $user->save(); 
       return $user;
    }
}

