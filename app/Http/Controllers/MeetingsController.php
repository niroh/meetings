<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting; 
use App\User;
use App\Participant;
use App\Organization;
use App\Subject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
class MeetingsController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        //$names = User::find($id)->name;
        $parts = User::find($id)->participants;
        //$parts_id=$parts->meeting_id;
        $meetings = Meeting::all();
        //$user= User::find($id)->organization_id;
        return view('meetings.index', compact('meetings','parts','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
            if (Gate::denies('manager') && Gate::denies('admin')) {
                abort(403,"Sorry you are not allowed to create meetings..");
            } 
            $id= Auth::id();
            $user = User::find($id);
            $orgid =$user->organization_id;
            $users = Organization::find($orgid)->users;
            $min_subject1=Organization::find($orgid);
            $min_subjects=$min_subject1->min_subject;
        return view ('meetings.create',compact('users','min_subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create meetings..");
        }
        $this->validate($request,[
            'title'=>"required|string|max:50",
            'start_hour'=>"required",
            'place'=>"required",
            'add_subjects' =>"required|array",
            'ending_hour' => "required|after:start_hour",
            'subject_hour.*' => "required|after:start_hour|before:ending_hour",
            'meeting_date' => "required|after:today",
        ]);
        $meetings = new Meeting();
        $id=Auth::id();
        $user=User::find($id);
        $meetings->title = $request->title;
        $meetings->meeting_date = $request->meeting_date;
        $meetings->start_hour = $request->start_hour;
        $meetings->ending_hour = $request->ending_hour;
        $meetings->place = $request->place;
        $meetings->user_id = $id;
        $meetings->organization_id = $user->organization_id;
        $meetings->save();

        $meet_id=$meetings->id;

        $participant=new Participant();
        $participant->user_id = $id;
        $participant->meeting_id = $meet_id;
        $participant->save();

         $participants = $request->input('add_participants');
           for ($i=0; $i < count($participants) ; $i++) { 
                $participant1 = new Participant();
                $participant1->user_id = $participants[$i];
                $participant1->meeting_id = $meet_id;
                $participant1->save();
           }
           $subjects = $request->input('add_subjects');
           $subject_time = $request->input('subject_hour');
           for ($i=0; $i < count($subjects) ; $i++) { 
                $subjects1 = new Subject();
                $subjects1->title = $subjects[$i];
                $subjects1->subject_time =$subject_time[$i];
                $subjects1->meeting_id = $meet_id;
                $subjects1->save();
           }
           $orguser=$user->organization_id;
           $org=Organization::find($orguser);
           $org->min_subject=$request->min_subject;
           $org->save();
           
        return redirect('meetings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meetings = Meeting::find($id);
        return view('meetings.edit', compact('meetings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to update meetings..");
        }

        $this->validate($request,[
            'title'=>"required|alpha|max:50",
            'meeting_date' => "required|after:today",
        ]);
        $meetings = Meeting::find($id);
        $meetings -> update($request->all());
        return redirect('meetings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete meetings..");
        }
        $meetings = Meeting::find($id);
        $meetings->delete();
        return redirect('meetings');
    }
}
