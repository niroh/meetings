<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Charts;
use App\User;
use DB;
use App\Organization;
use App\Task;
use Illuminate\Support\Facades\Auth;
class ChartController extends Controller
{
    //
    public function index()
    {
        $user=Auth::id();
        $tasks = DB::table('tasks')
                ->join('users','user_id','=','users.id')
                ->select('task_deadline',DB::raw('COUNT(task_deadline) as number'))
                ->where('tasks.user_id','=',$user)
                ->groupBy('task_deadline')->get();

        $chart = Charts::create('bar', 'highcharts')
			      ->title("Monthly tasks Assigned")
			      ->elementLabel("Total Tasks")
                  ->dimensions(1000, 500)
                  ->values($tasks->pluck('number'))
                  ->labels($tasks->pluck('task_deadline'))
                  ->responsive(true);

        $tasks2 = DB::table('tasks')
                ->select('status',DB::raw("(count(id)) as total"))
                ->where('user_id',$user)
                ->groupBy('status')->get();

         $pie  = Charts::create('pie', 'highcharts')
				    ->title('Task Completed')
				    ->labels($tasks2->pluck('status'))
                    ->values($tasks2->pluck('total'))
				    ->dimensions(1000,500)
                    ->responsive(true);

        
                    
        return view('chart',compact('chart','pie'));
    }
}