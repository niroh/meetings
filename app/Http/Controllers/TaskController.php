<?php

namespace App\Http\Controllers;
use App\Task;
use App\Meeting;
use App\User;
use App\Organization;
use App\Subject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {
        $user=Auth::id();
        //$tasks=Task::all();
        $tasks=Meeting::find($id)->tasks;
        $subjects=Meeting::find($id)->subject;
        return view('tasks.index', compact('tasks','id','user','subjects'));
    }
    /**public function indexuser()
    {
        $user=Auth::id();
        //$tasks=Task::all();
        $meetings=User::find($user)->meetings;
        $tasks=Meeting::find($meetings)->tasks;
        return view('tasks.index', compact('tasks','user'));
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create task..");
        }
        $id_user=Auth::id();
        $id1=$id;
        $org_users=Organization::find($id_user)->users;
        return view ('tasks.create',compact('id1','org_users'));
       // return view ('tasks.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id1)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create meetings..");
        }
        $this->validate($request,[
            'title'=>"required|alpha|max:50",
            'task_deadline'=>"required|after:today",
            'user_id'=>"required|integer",
        ]);
        $id2=$id1;
       // $id2=$request->id1;
        $orgid=Meeting::find($id2);
        $tasks = new Task();
        $tasks->title = $request->title;
        $tasks->meeting_id = $id2;
        $tasks->organization_id=$orgid->organization_id;
        $tasks->user_id=$request->user_id;
        $tasks->task_deadline=$request->task_deadline;
        $tasks->save();
        return redirect('meetings');
        //return redirect('tasks',compact('id1')) ;
        //return view('tasks.index', ['tasks'=>$tasks]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to edit task..");
        }
        $tasks = Task::find($id);
        $orgid = $tasks->organization_id;
        $meetings1 = Organization::find($orgid)->meetings;
        //$meetings = $meetings1->id;
        return view('tasks.edit', compact('tasks','meetings1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo $request;
        $task = Task::find($id);
        //if(!$tasks->user->id == Auth::id()) return(redirect('todos'));
        if($task->status==0){
            if(!$task->meeting->user_id == Auth::id()) return(redirect('showtasks'));
            $task -> update($request->except(['_token']));
            if($request->ajax()){
                return Response::json(array('result'=>'success', 'status'=>$request->status),200);
            }

         }
         $this->validate($request,[
            'title'=>"required|alpha|max:50",
            'meeting_id'=>"required|integer",
        ]);
       // $task -> update($request->all());
        return redirect('meetings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete task..");
        }
        $tasks = Task::find($id);
        $tasks->delete();
        return redirect('meetings');

    }
    public function mytasks(){
        $id=Auth::id();
        $tasks=User::find($id)->tasks_respo;
        return view('tasks.mytasks',compact('tasks'));
    }
    public function updatesubject($id){
        if (Gate::denies('manager') && Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to update subject..");
        }
        $subject=Subject::find($id);
        $subject->status =1;
        $subject->update();
        return redirect('meetings');
    }
  
}
