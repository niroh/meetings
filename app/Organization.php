<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

    protected $fillable = [
        'name', 'user_id','min_subject',
    ];

    public function users(){
        return $this->hasMany('App\User');
    }
    public function meetings(){
        return $this->hasMany('App\Meeting');
    }
}
