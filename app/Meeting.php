<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function tasks(){
        return $this->hasMany('App\Task');
    }
    public function organization(){
        return $this->belongsTo('App\Organization');
    }
    public function participant(){
        return $this->hasMany('App\Participant');
    }
    public function subject(){
        return $this->hasMany('App\Subject');
    }
    protected $fillable = [
        'title','meeting_date','organization_id','start_hour','ending_hour','place',
    ];


}
