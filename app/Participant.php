<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function meetings(){
        return $this->belongsTo('App\Meeting');
    }
    protected $fillable = [
        'user_id','meeting_id',
     ];
}
