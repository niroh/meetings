<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }
    protected $fillable = [
        'title','meeting_id','status',
    ];
}
