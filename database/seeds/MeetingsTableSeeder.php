<?php

use Illuminate\Database\Seeder;

class MeetingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meetings')->insert(
[
	[        
            'user_id' => 1,
            'subject' => 'test meeting',
            'meeting_time' => '2019-12-12 12:24:00',
            'created_at' => date('Y-m-d G:i:s'),
	],
        ]);
    }

}
