<?php

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert(
            [
                [
                        'name' => 'org1',
                        'user_id' => '1',
                        'min_subject'=>'2',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                    
                ]);
    }
}
